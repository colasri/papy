# PaPy

Paragliding stats and plots in python.

## Requirements

A few packages are needed, here installed in a new conda environment, here called `pypar`. Packages are installed with `conda` when available, and `aerofiles` with `pip` since I could not find a recent version on `conda`.

```bash
## Update conda, optional
# conda update -n base -c defaults conda

## Create an environment called "pypar" with most required packages
conda create -n pypar -c conda-forge -y python=3 jupyter pandas geopandas numpy matplotlib folium

## Enter this environment
conda activate pypar

## Install aerofiles to read igc and kml files
pip install aerofiles

## Open the main script
jupyter notebook papy.ipynb
```

## Notebooks

When in a notebook, execute a cell with shift+return.

## Preview

![example map picture](./map_preview.png)
